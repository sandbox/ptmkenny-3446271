# Firebase PHP

This module integrates [kreait/firebase-php](https://github.com/kreait/firebase-php) as a Drupal service.

kreait/firebase-php is an unofficial implementation of the Google Firebase Admin SDK for PHP.

This module is a community implementation and not affiliated with the kreait/firebase-php project. In other words, it is an unofficial implementation of an unofficial implementation. :)

This is a developer module and you will need to write code for it to be useful.

See the [kreait/firebase-php documentation](https://firebase-php.readthedocs.io/en/).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/firebase_php).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/firebase_php).


## Warning

This is an API module, but the module is not finalized. It could change at any time.

If you want stability, extend FirebasePhpMessagingServiceInterface and use the $messaging service from kreait.


## Update Policy

kreait/firebase-php is a fast-moving library. There is a major release nearly every year, and only the most recent version is supported.

So, this module will follow a similar update policy-- only the latest versions of Drupal and kreait/firebase-php will be supported.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. At `/admin/config/system/firebase_php`, set the credentials path.

### How to Get the Credentials File

You will need to generate a private key file in JSON format.

1. Open https://console.firebase.google.com/project/_/settings/serviceaccounts/adminsdk.
1. Select the project.
1. Click Generate New Private Key.
1. Confirm by clicking Generate Key.
1. Store the JSON file, which contains the key, in the private files directory (outside the webroot).

**Do NOT commit the key to your repository.**

If your private files directory is inside your webroot, move it outside your webroot. (If your host doesn't let you do this, get a new host.)


## FAQ (optional)

**Q: How can I send a test message?**

**A:** Go to `admin/config/system/firebase_php/test`.


## Maintainers

- Patrick Kenny - [ptmkenny](https://www.drupal.org/u/ptmkenny)
