<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Enum;

/**
 * Settings values for Firebase PHP.
 */
enum FirebasePhpSettings: string {
  case CredentialsPath = 'credentials_path';
  case LoggingLevel = 'logging_level';
}
