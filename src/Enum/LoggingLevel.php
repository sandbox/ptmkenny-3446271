<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Enum;

/**
 * Possible values for logging level for Firebase PHP.
 */
enum LoggingLevel: string {
  case Debug = 'debug';
  case Standard = 'standard';
  case None = 'none';
}
