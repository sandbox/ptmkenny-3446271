<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Enum;

/**
 * CloudMessage destinations.
 */
enum SendingDestinationType: string {
  case DeviceToken = 'token';
  case Topic = 'topic';
}
