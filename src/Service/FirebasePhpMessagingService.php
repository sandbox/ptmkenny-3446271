<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Session\AccountInterface;
use Drupal\firebase_php\Enum\FirebasePhpSettings;
use Drupal\firebase_php\Enum\LoggingLevel;
use Drupal\firebase_php\Exception\FirebasePhpCredentialsNotFoundException;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Factory;
use Psr\Log\LoggerInterface;

/**
 * Service for pushing messages to mobile devices using Firebase PHP.
 *
 * If you want to work with firebase-php directly, you can extend this service.
 *
 * If you want to use this module's helper functions, see
 * FirebasePhpMessagingApi.
 */
class FirebasePhpMessagingService {

  /**
   * The messaging service from the Firebase PHP Admin SDK.
   *
   * @var \Kreait\Firebase\Contract\Messaging
   */
  public Messaging $messaging;

  /**
   * This module's configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  public function __construct(
    protected LoggerInterface $logger,
    protected LoggerInterface $loggerDebug,
    protected AccountInterface $account,
    protected ConfigFactory $configFactory,
  ) {
    $config = $this->configFactory->get('firebase_php.settings');
    $credentials = $config->get(FirebasePhpSettings::CredentialsPath->value);
    if ($credentials === NULL) {
      throw new FirebasePhpCredentialsNotFoundException(
        "Failed to get Firebase credentials!",
      );
    }

    $factory = (new Factory)->withServiceAccount($credentials);

    $logging_level = LoggingLevel::from($config->get(FirebasePhpSettings::LoggingLevel->value));

    switch ($logging_level) {
      case LoggingLevel::Debug:
        $factory->withHttpDebugLogger($this->loggerDebug);
      case LoggingLevel::Standard:
        $factory->withHttpLogger($this->logger);
        break;
    }

    $this->messaging = $factory->createMessaging();
    $this->config = $config;
  }

}
