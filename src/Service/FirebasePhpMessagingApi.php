<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Service;

use Drupal\firebase_php\Enum\SendingDestinationType;
use Drupal\firebase_php\Exception\FirebasePhpException;
use Drupal\firebase_php\Exception\FirebasePhpInvalidArgumentException;
use Drupal\firebase_php\Exception\FirebasePhpInvalidTokenException;
use Drupal\firebase_php\FirebasePhpMessagingApiInterface;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Messaging\ApnsConfig;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\MulticastSendReport;
use Kreait\Firebase\Messaging\Notification;

/**
 * Provides a Drupal API for firebase-php.
 *
 * Totally optional! You can use firebase-php however you want by extending
 * FirebasePhpMessagingService, just like this API does.
 */
class FirebasePhpMessagingApi extends FirebasePhpMessagingService implements FirebasePhpMessagingApiInterface {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getMessaging(): Messaging {
    return $this->messaging;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function sendMessageSingleDevice(
    string $device_token,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): array {
    $notification = $this->createNotification($notification_title, $notification_body, $notification_url);
    $message_array = $this->createMessageSingleDevice($device_token, $notification, $data);
    $cloud_message = $this->createCloudMessage($message_array, $badge_count, $apns_data);

    return $this->sendCloudMessage($cloud_message);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function sendMessageTopic(
    string $topic,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): array {
    $notification = $this->createNotification($notification_title, $notification_body, $notification_url);
    $message_array = $this->createMessageTopic($topic, $notification, $data);
    $cloud_message = $this->createCloudMessage($message_array, $badge_count, $apns_data);

    return $this->sendCloudMessage($cloud_message);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function sendMessageMultipleDevices(
    array $device_tokens,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): MulticastSendReport {
    $notification = $this->createNotification($notification_title, $notification_body, $notification_url);
    // For multicast, create a message to the first device token.
    $message_array = $this->createMessageSingleDevice($device_tokens[0], $notification, $data);
    // Then send that message to all the device tokens.
    $message = $this->createCloudMessage($message_array, $badge_count, $apns_data);
    return $this->messaging->sendMulticast($message, $device_tokens);
  }

  /**
   * Creates a notification for use in sending messages.
   *
   * All messages need a notification.
   *
   * @param string|null $title
   *   The title of the notification.
   * @param string|null $body
   *   The body of the notification.
   * @param string|null $url
   *   The optional URL for the notification.
   *
   * @return \Kreait\Firebase\Messaging\Notification
   *   The notification.
   */
  private function createNotification(?string $title, ?string $body, ?string $url = NULL): Notification {
    return Notification::create($title, $body, $url);
  }

  /**
   * Prepares an array for CloudMessage::fromArray().
   *
   * @param \Kreait\Firebase\Messaging\Notification $notification
   *   The notification to be sent.
   * @param \Drupal\firebase_php\Enum\SendingDestinationType $destination_type
   *   The type of destination to send the message to. Used for validation.
   * @param string $destination
   *   The destination (deviceToken or topic).
   * @param array|null $data
   *   The optional message data.
   *
   * @return array
   *   An array ready for CloudMessage::fromArray().
   */
  private function createMessageArray(
    Notification $notification,
    SendingDestinationType $destination_type,
    string $destination,
    ?array $data = NULL,
  ): array {
    $message_inputs = [
      'notification' => $notification,
    ];
    if (is_array($data)) {
      $message_inputs['data'] = $data;
    }

    if (trim($destination) !== '') {
      /** @var non-empty-string $destination */
      $message_inputs[$destination_type->value] = $destination;
    }
    else {
      throw new FirebasePhpInvalidArgumentException('Either device token or topic must be provided.');
    }

    return $message_inputs;
  }

  /**
   * Creates a message for a single device.
   */
  private function createMessageSingleDevice(string $deviceToken, Notification $notification, ?array $data = NULL): array {
    return $this->createMessageArray($notification, SendingDestinationType::DeviceToken, $deviceToken, $data);
  }

  /**
   * Creates a message for a topic.
   */
  private function createMessageTopic(string $topic, Notification $notification, ?array $data = NULL): array {
    return $this->createMessageArray($notification, SendingDestinationType::Topic, $topic, $data);
  }

  /**
   * Creates a CloudMessage to be sent.
   *
   * @param array $message_inputs
   *   An array generated by $this->createMessageArray().
   * @param int|null $badge_count
   *   The optional badge count.
   * @param array|null $apns_data
   *   The optional APNS data.
   *
   * @return \Kreait\Firebase\Messaging\CloudMessage
   *   The message to be sent.
   */
  private function createCloudMessage(
    array $message_inputs,
    ?int $badge_count = NULL,
    ?array $apns_data = NULL,
  ): CloudMessage {
    $message = CloudMessage::fromArray($message_inputs);

    if (is_array($apns_data)) {
      $message = $message->withApnsConfig($apns_data);
    }

    if (is_int($badge_count) && $badge_count >= 0) {
      $message = $message->withApnsConfig(ApnsConfig::new()->withBadge($badge_count));
    }

    return $message;
  }

  /**
   * Sends a cloud message.
   *
   * @param \Kreait\Firebase\Messaging\CloudMessage $cloud_message
   *   The cloud message to send.
   *
   * @return array
   *   The result from the kreait/firebase-php library.
   */
  private function sendCloudMessage(CloudMessage $cloud_message): array {
    try {
      return $this->messaging->send($cloud_message);
    }
    catch (InvalidMessage $e) {
      if ($e->errors()['error']['code'] === 400) {
        throw new FirebasePhpInvalidTokenException('Invalid registration token. Deletion recommended.', 400, $e);
      }
      throw new FirebasePhpException('Failed to send cloud message!', 404, $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validateToken(string $token): bool {
    if (trim($token) === '') {
      throw new FirebasePhpInvalidArgumentException('Token cannot be an empty string!');
    }
    /** @var non-empty-string $token */
    $messaging_service = $this->getMessaging();
    $output = $messaging_service->validateRegistrationTokens($token);
    // We are only validating one token, so if there is a single invalid result,
    // the token is invalid.
    return !isset($output['invalid'][0]);
  }

}
