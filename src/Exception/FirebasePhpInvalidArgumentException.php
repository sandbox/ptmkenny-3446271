<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Exception;

/**
 * Exception if the API is called with an invalid argument.
 */
class FirebasePhpInvalidArgumentException extends FirebasePhpException {
}
