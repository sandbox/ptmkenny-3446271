<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Exception;

/**
 * Exception if the token is invalid according to Google.
 *
 * If you get this exception, the token can be deleted.
 */
class FirebasePhpInvalidTokenException extends FirebasePhpException {
}
