<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Exception;

/**
 * Exception if the JSON credentials cannot be found.
 */
class FirebasePhpCredentialsNotFoundException extends FirebasePhpException {
}
