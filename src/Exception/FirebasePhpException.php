<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Exception;

/**
 * Exceptions for the firebase_php module.
 */
class FirebasePhpException extends \Exception {
}
