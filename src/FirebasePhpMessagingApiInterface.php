<?php

declare(strict_types=1);

namespace Drupal\firebase_php;

use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Messaging\MulticastSendReport;

/**
 * Provides an interface for working with the Firebase PHP Messaging Service.
 *
 * THIS MODULE IS IN DEVELOPMENT AND THIS API WILL CHANGE!
 */
interface FirebasePhpMessagingApiInterface {

  /**
   * Gets the messaging service for custom coding.
   *
   * Use this to add whatever actions you want.
   *
   * For more info, see the Kreait Firebase PHP docs.
   *
   * @return \Kreait\Firebase\Contract\Messaging
   *   The messaging service from the Firebase PHP Admin SDK.
   */
  public function getMessaging(): Messaging;

  /**
   * Sends a message to a single device.
   *
   * @param string $device_token
   *   The token of the device to send a message to.
   * @param string|null $notification_title
   *   The title of the notification.
   * @param string|null $notification_body
   *   The body of the notification.
   * @param string|null $notification_url
   *   The optional notification URL.
   * @param array|null $data
   *   The optional data.
   * @param array|null $apns_data
   *   The optional APNS data.
   * @param int|null $badge_count
   *   The optional badge count for the device.
   *
   * @return array
   *   The array from firebase-php messaging->send().
   */
  public function sendMessageSingleDevice(
    string $device_token,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): array;

  /**
   * Sends a message to a topic.
   *
   * @param string $topic
   *   The topic.
   * @param string|null $notification_title
   *   The title of the notification.
   * @param string|null $notification_body
   *   The body of the notification.
   * @param string|null $notification_url
   *   The optional notification URL.
   * @param array|null $data
   *   The optional data.
   * @param array|null $apns_data
   *   The optional APNS data.
   * @param int|null $badge_count
   *   The optional badge count for the device.
   *
   * @return array
   *   The array from firebase-php messaging->send().
   */
  public function sendMessageTopic(
    string $topic,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): array;

  /**
   * Sends a single message to multiple devices.
   *
   * @param array $device_tokens
   *   An array of all the device tokens to send the message to.
   * @param string|null $notification_title
   *   The title of the notification.
   * @param string|null $notification_body
   *   The body of the notification.
   * @param string|null $notification_url
   *   The optional notification URL.
   * @param array|null $data
   *   The optional data.
   * @param array|null $apns_data
   *   The optional APNS data.
   * @param int|null $badge_count
   *   The optional badge count for the device.
   *
   * @return \Kreait\Firebase\Messaging\MulticastSendReport
   *   A report of message showing whether message sending succeeded or failed.
   */
  public function sendMessageMultipleDevices(
    array $device_tokens,
    ?string $notification_title,
    ?string $notification_body,
    ?string $notification_url = NULL,
    ?array $data = NULL,
    ?array $apns_data = NULL,
    ?int $badge_count = NULL,
  ): MulticastSendReport;

  /**
   * Validates a registration token with Google's servers.
   *
   * @param string $token
   *   The registration token to validate.
   *
   * @return bool
   *   TRUE if the token is valid. FALSE if not.
   */
  public function validateToken(string $token): bool;

}
