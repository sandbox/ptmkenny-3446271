<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\firebase_php\Enum\FirebasePhpSettings;
use Drupal\firebase_php\Enum\LoggingLevel;

/**
 * Configures the Firebase PHP Admin SDK.
 */
class FirebasePhpConfigurationForm extends ConfigFormBase {

  const FORM_ID = 'firebase_php.settings';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [self::FORM_ID];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::FORM_ID);

    $form[self::FORM_ID] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Firebase'),
      '#open' => TRUE,
    ];

    $form[self::FORM_ID][FirebasePhpSettings::CredentialsPath->value] = [
      '#type' => 'textfield',
      '#title' => $this->t('Credentials Path'),
      '#description' => $this->t(
        'The path to the JSON credentials file. SECURITY WARNING: This file MUST be outside the Drupal webroot. The path may be absolute (e.g., %abs), relative to the Drupal directory (e.g., %rel), or defined using a stream wrapper (e.g., %str).', [
          '%abs' => '/etc/foobar.json',
          '%rel' => '../firebase/foobar.json',
          '%str' => 'private://firebase/foobar.json',
        ]),
      '#default_value' => $config->get(FirebasePhpSettings::CredentialsPath->value),
      '#required' => TRUE,
    ];

    $form[self::FORM_ID][FirebasePhpSettings::LoggingLevel->value] = [
      '#type' => 'select',
      '#title' => $this->t('Logging Level'),
      '#description' => $this->t('Choose the level of detail to be logged to Drupal.'),
      '#options' => $this->getLoggingOptions(),
      '#default_value' => $config->get(FirebasePhpSettings::LoggingLevel->value),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(self::FORM_ID);
    $config
      ->set(FirebasePhpSettings::CredentialsPath->value, $form_state->getValue(FirebasePhpSettings::CredentialsPath->value))
      ->set(FirebasePhpSettings::LoggingLevel->value, $form_state->getValue(FirebasePhpSettings::LoggingLevel->value))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $settings = $form_state->getValues();
    $file_path = $settings[FirebasePhpSettings::CredentialsPath->value];

    // Does the file exist?
    if (!is_file($file_path)) {
      $form_state->setErrorByName('file_location', $this->t('There is no file at the specified location.'));
      return;
    }

    // Is the file readable?
    if ((!is_readable($file_path))) {
      $form_state->setErrorByName('file_location', $this->t('The file at the specified location is not readable.'));
      return;
    }

    // Is it obviously insecure?
    if (str_starts_with('public://', (string) $file_path)) {
      $form_state->setErrorByName('path_invalid', $this->t('You cannot use a public file. That is insecure.'));
      return;
    }

  }

  /**
   * Get the logging level options.
   *
   * @return array
   *   List of options, keyed by logging level.
   */
  protected function getLoggingOptions(): array {
    return [
      LoggingLevel::Debug->value => LoggingLevel::Debug->value,
      LoggingLevel::Standard->value => LoggingLevel::Standard->value,
      LoggingLevel::None->value => LoggingLevel::None->value,
    ];
  }

}
