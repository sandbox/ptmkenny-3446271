<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\firebase_php\FirebasePhpMessagingApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to validate tokens for sending push notifications.
 */
final class FirebasePhpValidateTokenForm extends FormBase {

  public function __construct(
    protected FirebasePhpMessagingApiInterface $messagingService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): FirebasePhpValidateTokenForm {
    return new self(
      $container->get('firebase_php.messaging_drupal_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'firebase_php_validate_token_form';
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['device_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Device tokens'),
      '#required' => TRUE,
      '#description' => $this->t('Enter as many push notification tokens as you like, separated by commas.'),
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      $tokens_string = $form_state->getValue('device_token');
      // array_filter removes the empty strings.
      /** @var non-empty-string[] $tokens_array */
      $tokens_array = array_filter(explode(',', (string) $tokens_string));
      $output = $this->messagingService->getMessaging()->validateRegistrationTokens($tokens_array);

      $output_message = $this->t('Device token validation results: %output', [
        '%output' => json_encode($output),
      ]);

      // If there is an invalid token, show an error.
      if (isset($output['invalid'][0]) || isset($output['unknown'][0])) {
        $this->messenger()->addError($output_message);
      }
      else {
        $this->messenger()->addStatus($output_message);
      }
    }
    // @phpstan-ignore-next-line Exception is shown to admin, so no rethrowing.
    catch (\Exception $e) {
      Error::logException($this->logger('firebase_php'), $e);
      $this->messenger()->addError($this->t('Failed to validate the token. Error: %error', [
        '%error' => $e->getMessage(),
      ]));
    }
  }

}
