<?php

declare(strict_types=1);

namespace Drupal\firebase_php\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\firebase_php\FirebasePhpMessagingApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to send a test push notification.
 */
final class FirebasePhpSendTestMessageForm extends FormBase {

  public function __construct(
    protected FirebasePhpMessagingApiInterface $messagingService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): FirebasePhpSendTestMessageForm {
    return new self(
      $container->get('firebase_php.messaging_drupal_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'firebase_php_send_test_message_form';
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->t('Exciting test message! 🥳'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#default_value' => $this->t('This is a test message. It includes an emoji. 😄'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['device_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Device token'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['badge_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Badge count'),
      '#required' => FALSE,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      $this->messagingService->sendMessageSingleDevice(
        $form_state->getValue('device_token'),
        $form_state->getValue('title'),
        $form_state->getValue('message'),
        NULL,
        NULL,
        NULL,
        intval($form_state->getValue('badge_count')),
      );
      $this->messenger()->addStatus($this->t('The test message has been sent.'));
    }
    // @phpstan-ignore-next-line Exception is shown to admin, so no rethrowing.
    catch (\Exception $e) {
      Error::logException($this->logger('firebase_php'), $e);

      // Try to get more information for the developer.
      $details = 'No details available.';
      if (method_exists($e, 'errors')) {
        $details = $e->errors();
      }
      elseif ($e->getPrevious() instanceof \Throwable) {
        $details = $e->getPrevious()->getMessage();
      }

      $this->messenger()->addError($this->t('Failed to send the test message. Error: %error More info: %errorDetails', [
        '%error' => $e->getMessage(),
        '%errorDetails' => json_encode($details),
      ]));
    }
  }

}
